# README #

1/3スケールのSEGA マークⅢ風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- SEGA

## 発売時期
- 1985年10月20日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%BB%E3%82%AC%E3%83%BB%E3%83%9E%E3%83%BC%E3%82%AFIII)
- [セガハード大百科](https://sega.jp/history/hard/segamark3/index.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_sega_mark3/raw/5399f4fdc5a0a52e1d55ef4a086be8b1d28e72e2/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_sega_mark3/raw/5399f4fdc5a0a52e1d55ef4a086be8b1d28e72e2/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_sega_mark3/raw/5399f4fdc5a0a52e1d55ef4a086be8b1d28e72e2/ExampleImage.jpg)
